#import "HomeVC.h"
#import "SignInVC.h"

@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
        
    [self.navigationController setNavigationBarHidden:true];
    
    [super viewWillAppear:true];
        
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logOutButtonAction:(UIButton *)sender; {
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    SignInVC *vc = [[SignInVC alloc] init];
    [self.navigationController pushViewController:vc animated:true];   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
