#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>


@interface ViewController ()
    
    @end

@implementation ViewController
    
- (void)viewDidLoad {
    
    //Google
    [GPPSignIn sharedInstance].delegate = self;
    
    /*FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
     // Optional: Place the button in the center of your view.
     loginButton.center = self.view.center;
     [self.view addSubview:loginButton];
     loginButton.readPermissions = @[@"public_profile", @"email"];*/ //1st way FB
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
    
- (void)viewWillAppear:(BOOL)animated {
    /*if ([FBSDKAccessToken currentAccessToken]) {
     NSLog(@"View willl appear");
     // User is logged in, do work such as go to next view controller.
     }*/ //1st way
    [super viewWillAppear:true];
}
    
#pragma mark - FB
-(void)fetchUserInfo { //FB
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name,first_name,last_name,friendlists,picture"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error)
         {
             FBDataDict = [NSMutableDictionary new];
             [FBDataDict addEntriesFromDictionary:result];
             NSLog(@"Facebook User details: %@",FBDataDict);
             [self performSelector:@selector(checkFbLoggedin) withObject:Nil afterDelay:1.0];
         }
         else
         {
             NSLog(@"Error %@",error);
         }
     }];
}
    
- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{ //FB
    NSLog(@"LOGGED IN TO FACEBOOK");
    [self fetchUserInfo];
}
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton; //FB
    {
        NSLog(@"LOGGED OUT IN TO FACEBOOK");
    }
    
-(void)checkFbLoggedin { //FB
    if ([FBSDKAccessToken currentAccessToken]) { // To check whether the Facebook user is LoggedIn or not
        //User logged in successfully, go to next screen
    }
}

#pragma mark - Facebook Login Btn Action
- (IBAction)LoginBtnAction { //FB
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            NSLog(@"error %@",error);
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"Cancelled");
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                [self fetchUserInfo];
            }
        }
    }];
}
    
#pragma mark - Google SignIn
- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation {
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}
    
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if (error) {
        // Do some error handling here.
    } else {
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        NSLog(@"email %@ ", [NSString stringWithFormat:@"Email: %@",[GPPSignIn sharedInstance].authentication.userEmail]);
        NSLog(@"Received error %@ and auth object %@",error, auth);
        
        // 1. Create a |GTLServicePlus| instance to send a request to Google+.
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
        plusService.retryEnabled = YES;
        
        // 2. Set a valid |GTMOAuth2Authentication| object as the authorizer.
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        // 3. Use the "v1" version of the Google+ API.*
        plusService.apiVersion = @"v1";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        //Handle Error
                    } else {
                        NSLog(@"Google User Email= %@", [GPPSignIn sharedInstance].authentication.userEmail);
                        NSLog(@"Google User GoogleID=%@", person.identifier);
                        NSLog(@"Google User User Name=%@", [person.name.givenName stringByAppendingFormat:@" %@", person.name.familyName]);
                        NSLog(@"Google User Gender=%@", person.gender);
                    }
                }];
        
        //[self refreshInterfaceBasedOnSignIn];
    }
}
    
- (void)signOut {
    [[GPPSignIn sharedInstance] signOut];
}
    
- (void)disconnect {
    [[GPPSignIn sharedInstance] disconnect];
}
    
- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
    }
}
    
-(void)signIn:(GPPSignIn *)signIn didSignInForUser:(GPPSignInButton *)user withError:(NSError *)error{
    NSLog(@"Google sign user info: %@", user);
}

#pragma mark - Google Login Btn Action
- (IBAction)GoogleLoginBtnAction {
    
    [self signOut]; //To signOut Google user
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserID = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.delegate = self;
    
    if ([signIn hasAuthInKeychain])
    {
        [signIn trySilentAuthentication];
    }
    else
    [signIn authenticate];
    
    if ([[GPPSignIn sharedInstance] authentication]) {
        // The user is signed in.
        // Perform other actions here, such as showing a sign-out button
    } else {
        // Perform other actions here
    }
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    
    @end
