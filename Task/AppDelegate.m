#import "AppDelegate.h"
//FB
#import <FBSDKCoreKit/FBSDKCoreKit.h>

//Google
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

#import "SignInVC.h"
#import "HomeVC.h"

#import "Toast/UIView+Toast.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //FB
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    //Google+
    // Set app's client ID for |GPPSignIn| and |GPPShare|.
    [GPPSignIn sharedInstance].clientID = @"1096768915259-n4jqkcgrur58gq95e3ntbcrf3o7il113.apps.googleusercontent.com"; //Client ID will get in Google Account

    [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) { //Bool Value True
        HomeVC *VC = [[HomeVC alloc] initWithNibName:nil bundle:nil];
        self.navController = [[UINavigationController alloc] initWithRootViewController:VC];
    } else {
        SignInVC *VC = [[SignInVC alloc] initWithNibName:nil bundle:nil];
        self.navController = [[UINavigationController alloc] initWithRootViewController:VC];
    }
    
    self.window.rootViewController = self.navController;
    //[self.navController setNavigationBarHidden:true]; //To hide Navigation bar
    [self.window makeKeyAndVisible];
    
    return YES;
}

    //FB and Google
    - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
    {
        
        return ([[FBSDKApplicationDelegate sharedInstance] application:application
                                                               openURL:url
                                                     sourceApplication:sourceApplication
                                                            annotation:annotation
                 ] || [GPPURLHandler handleURL:url
                             sourceApplication:sourceApplication
                                    annotation:annotation]);
        
    }
    
    //Only FB
    /*- (BOOL)application:(UIApplication *)application
                openURL:(NSURL *)url
      sourceApplication:(NSString *)sourceApplication
             annotation:(id)annotation {
        
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
                        ];
        // Add any custom logic here.
        return handled;
    }*/
    
    //Only Google
    /*- (BOOL)application:(UIApplication *)application
                openURL:(NSURL *)url
      sourceApplication:(NSString *)sourceApplication
             annotation:(id)annotation {
        // We need to use the helper GPPURLHandler class to manage the open URL. This
        // is used for the response from the Google+ application or the browser after
        // a user has been taken away to authorise an application. It also handles
        // callbacks after sharing, and if the application is opened with a deeplink!
        //
        // If you need to combine this with other URL handlers, you can just test the
        // returned BOOL, and hand over to the next handler in the chain if the return
        // is NO.
        return [GPPURLHandler handleURL:url
                      sourceApplication:sourceApplication
                             annotation:annotation];
    }*/
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

    
    @end
