#import "SignInVC.h"
#import "SignUpVC.h"
#import "Toast/UIView+Toast.h"

#import "HomeVC.h"

@interface SignInVC ()
    
@end

@implementation SignInVC
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
    
- (void)viewWillAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:true];
    
    [super viewWillAppear:true];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (IBAction)signInButtonAction:(UIButton *)sender {
    if(userNameTF.text.length == 0) {
        [self.view makeToast:@"Please enter User name" duration:0.5 position:CSToastPositionTop];
        return;
    } else if(passwordTF.text.length == 0) {
        [self.view makeToast:@"Please enter Password" duration:0.5 position:CSToastPositionTop];
        return;
    } else {
        //Hit Login service
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        HomeVC *vc = [[HomeVC alloc] init];
        [self.navigationController pushViewController:vc animated:true];
    }
}
    
- (IBAction)signUPButtonAction:(UIButton *)sender; {
    SignUpVC *vc = [[SignUpVC alloc] init];
    [self.navigationController pushViewController:vc animated:true];       
}

@end
