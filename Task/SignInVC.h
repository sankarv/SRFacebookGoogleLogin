#import <UIKit/UIKit.h>

@interface SignInVC : UIViewController<UITextFieldDelegate>
{
    __weak IBOutlet UITextField *userNameTF;
    
    __weak IBOutlet UITextField *passwordTF;
}
- (IBAction)signInButtonAction:(UIButton *)sender;
- (IBAction)signUPButtonAction:(UIButton *)sender;
    
@end
