#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>

@class GPPSignInButton;

@interface SignUpVC : UIViewController<GPPSignInDelegate>
{
    NSMutableDictionary *FBDataDict;
        
    GPPSignIn *signIn;
    
    __weak IBOutlet UITextField *userNameTF;
    __weak IBOutlet UITextField *emailTF;
    __weak IBOutlet UITextField *phoneNoTF;
    __weak IBOutlet UITextField *passwordTF;
    __weak IBOutlet UITextField *confirmPasswordTF;
}
    
- (IBAction)signUPButtonAction:(UIButton *)sender;
- (IBAction)facebookButtonAction:(UIButton *)sender;
- (IBAction)googleButtonAction:(UIButton *)sender;
    @end
