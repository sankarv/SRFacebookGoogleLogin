#import "SignUpVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

#import "Toast/UIView+Toast.h"

#import "HomeVC.h"

@interface SignUpVC ()
    
    @end

@implementation SignUpVC
    
- (void)viewDidLoad {
    
    //Google
    [GPPSignIn sharedInstance].delegate = self;
    
    /*FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
     // Optional: Place the button in the center of your view.
     loginButton.center = self.view.center;
     [self.view addSubview:loginButton];
     loginButton.readPermissions = @[@"public_profile", @"email"];*/ //1st way FB
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
    
- (void)viewWillAppear:(BOOL)animated {
    /*if ([FBSDKAccessToken currentAccessToken]) {
     NSLog(@"View willl appear");
     // User is logged in, do work such as go to next view controller.
     }*/ //1st way
    
    [self.navigationController setNavigationBarHidden:false];
    [super viewWillAppear:true];
}
    
#pragma mark - FB
-(void)fetchUserInfo { //FB
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name,first_name,last_name,friendlists,picture"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error)
         {
             FBDataDict = [NSMutableDictionary new];
             [FBDataDict addEntriesFromDictionary:result];
             NSLog(@"Facebook User details: %@",FBDataDict);
             [self performSelector:@selector(checkFbLoggedin) withObject:Nil afterDelay:1.0];
         }
         else
         {
             NSLog(@"Error %@",error);
         }
     }];
}
    
- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{ //FB
    NSLog(@"LOGGED IN TO FACEBOOK");
    [self fetchUserInfo];
}
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton; //FB
    {
        NSLog(@"LOGGED OUT IN TO FACEBOOK");
    }
    
-(void)checkFbLoggedin { //FB
    if ([FBSDKAccessToken currentAccessToken]) { // To check whether the Facebook user is LoggedIn or not
        //User logged in successfully, go to next screen
        [self openHomeScreen];
    }
}
    
#pragma mark - Facebook Login Btn Action
- (IBAction)facebookButtonAction:(UIButton *)sender; { //FB
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            NSLog(@"error %@",error);
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"Cancelled");
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                [self fetchUserInfo];
            }
        }
    }];
}
    
#pragma mark - Google SignIn
- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation {
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}
    
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if (error) {
        // Do some error handling here.
    } else {
        
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        NSLog(@"email %@ ", [NSString stringWithFormat:@"Email: %@",[GPPSignIn sharedInstance].authentication.userEmail]);
        NSLog(@"Received error %@ and auth object %@",error, auth);
        
        // 1. Create a |GTLServicePlus| instance to send a request to Google+.
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
        plusService.retryEnabled = YES;
        
        // 2. Set a valid |GTMOAuth2Authentication| object as the authorizer.
        [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
        
        // 3. Use the "v1" version of the Google+ API.*
        plusService.apiVersion = @"v1";
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        //Handle Error
                    } else {
                        NSLog(@"Google User Email= %@", [GPPSignIn sharedInstance].authentication.userEmail);
                        NSLog(@"Google User GoogleID=%@", person.identifier);
                        NSLog(@"Google User User Name=%@", [person.name.givenName stringByAppendingFormat:@" %@", person.name.familyName]);
                        NSLog(@"Google User Gender=%@", person.gender);
                    }
                }];
        
        [self openHomeScreen];
        //[self refreshInterfaceBasedOnSignIn];
    }
}
    
- (void)signOut {
    [[GPPSignIn sharedInstance] signOut];
}
    
- (void)disconnect {
    [[GPPSignIn sharedInstance] disconnect];
}
    
- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        // The user is signed out and disconnected.
        // Clean up user data as specified by the Google+ terms.
    }
}
    
-(void)signIn:(GPPSignIn *)signIn didSignInForUser:(GPPSignInButton *)user withError:(NSError *)error{
    NSLog(@"Google sign user info: %@", user);
}
    
#pragma mark - Google Login Btn Action
- (IBAction)googleButtonAction:(UIButton *)sender; {    
    [self signOut]; //To signOut Google user
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserID = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.delegate = self;
    
    if ([signIn hasAuthInKeychain])
    {
        [signIn trySilentAuthentication];
    }
    else
    [signIn authenticate];
    
    if ([[GPPSignIn sharedInstance] authentication]) {
        // The user is signed in.
        // Perform other actions here, such as showing a sign-out button
    } else {
        // Perform other actions here
    }
}
    
#pragma mark - SignUp Btn Action
- (IBAction)signUPButtonAction:(UIButton *)sender; {
    if(userNameTF.text.length == 0) {
        [self.view makeToast:@"Please enter User name" duration:0.5 position:CSToastPositionTop];
        return;
    } else if(emailTF.text.length == 0) {
        [self.view makeToast:@"Please enter Email" duration:0.5 position:CSToastPositionTop];
        return;
    } else if(![self validateEmail:emailTF.text]) {
        [self.view makeToast:@"Please enter Valid Email" duration:0.5 position:CSToastPositionTop];
        return;
    } else if(phoneNoTF.text.length == 0) {
        [self.view makeToast:@"Please enter Phone number" duration:0.5 position:CSToastPositionTop];
        return;
    } else if(passwordTF.text.length == 0) {
        [self.view makeToast:@"Please enter Password" duration:0.5 position:CSToastPositionTop];
        return;
    } else if(confirmPasswordTF.text.length == 0) {
        [self.view makeToast:@"Please enter Confirm Password" duration:0.5 position:CSToastPositionTop];
        return;
    } else if([confirmPasswordTF.text isEqualToString:passwordTF.text]) {
        [self.view makeToast:@"Please enter Pssword and Confirm Password should be same" duration:0.5 position:CSToastPositionTop];
        return;
    } else {
        //Hit SignUp service
        [self openHomeScreen];   
    }
}
    
#pragma mark - validate email
-(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0)
    {
        return NO;
    }
    else
    return YES;
}

    /*-(BOOL)textFieldShouldReturn:(UITextField *)textField
    {
        if (textField==txtname) {
            [lastName becomeFirstResponder];
            return NO;
        }
        if (textField==lastName) {
            [txtphone becomeFirstResponder];
            
            return NO;
        }
        if (textField==txtphone) {
            [txtemail becomeFirstResponder];
            return NO;
        }
        if (textField==txtemail) {
            [txtpassword becomeFirstResponder];
            return NO;
        }
        if (textField==txtpassword) {
            [self list:Nil];
            [textField resignFirstResponder];
            mainScroll.contentOffset=CGPointMake(0, 0);
            return YES;
        }
        return YES;
    }*/
- (void)openHomeScreen {
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    HomeVC *vc = [[HomeVC alloc] init];
    [self.navigationController pushViewController:vc animated:true];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    @end
